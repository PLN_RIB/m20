report 1017 "ImportPostCodesModified"
{

    Caption = 'Import Post Codes';
    ProcessingOnly = true;
    UseRequestPage = false;

    /// gui allowed 

    dataset
    {
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPreReport()
    var
        FileManagement: Codeunit "File Management";
        TempServerFilePath: Text;
        FileName: Text;
        TitleTxt: Label 'Upload CSV or ZIP file';
        EntriesWillBeDeletedQst: Label 'Import starten?';
        ImportSuccessfulMsg: Label 'Import erfolgreich!';
    begin
        if FileName = '' then
            TempServerFilePath := FileManagement.UploadFile(TitleTxt, '*.*')
        else
            TempServerFilePath := FileName;
        if TempServerFilePath = '' then
            Error('');
        if not Confirm(EntriesWillBeDeletedQst, true) then
            Error('');
        ImportPostCodes(TempServerFilePath);
        FileManagement.DeleteServerFile(TempServerFilePath);
        Message(ImportSuccessfulMsg);
    end;

    procedure ImportPostCodes(TempServerFilePath: Text)
    var
        TempCSVBuffer: Record "CSV Buffer" temporary;
        TempBlob: Codeunit "Temp Blob";
        FileManagement: Codeunit "File Management";
        DataCompression: Codeunit "Data Compression";
        EntryList: List of [Text];
        OutStream: OutStream;
        OriginalInStream: InStream;
        ExtractedInStream: InStream;
        TempServerFile: File;
        Window: Dialog;
        NumberOfPostCodes: Integer;
        Length: Integer;
        EntriesWillBeDeletedQst: Label 'Entries will be deleted.';
        PostCode: Record "Post Code";
        ImportSuccessfulMsg: Label 'The new post codes have been successfully imported.';
        FileName: Text;
        TitleTxt: Label 'Upload CSV or ZIP file';
        StatusMsg: Label 'Number of post codes imported: #1#########', Comment = '#1##: Number of imported post codes';
    begin
        Clear(OriginalInStream);
        Window.Open(StatusMsg);
        PostCode.Reset();
        ///PostCode.SetRange(Code, '1000', '9999');
        //PostCode.DeleteAll(); //multiple parts
        ///PostCode.Reset();

        TempServerFile.Open(TempServerFilePath);
        TempServerFile.CreateInStream(OriginalInStream);
        if FileManagement.GetExtension(TempServerFilePath) <> 'zip' then
            Xmlport.Import(72206, OriginalInStream)///TempCSVBuffer.InitializeReaderFromStream(OriginalInStream, ';')
        else begin
            DataCompression.OpenZipArchive(OriginalInStream, false);
            DataCompression.GetEntryList(EntryList);
            if EntryList.Count() > 0 then begin

                TempBlob.CreateOutStream(OutStream);
                DataCompression.ExtractEntry(EntryList.Get(1), OutStream, Length);
                TempBlob.CreateInStream(ExtractedInStream);
                Xmlport.Import(72206, ExtractedInStream);///TempCSVBuffer.InitializeReaderFromStream(ExtractedInStream, ';');
                DataCompression.CloseZipArchive()
            end
        end
    end;

    procedure InitializeRequest(ServerFileName: Text)
    var
        FileName: Text;
    begin
        FileName := ServerFileName;
    end;
}


