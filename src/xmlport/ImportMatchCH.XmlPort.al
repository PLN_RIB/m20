xmlport 72206 "Import Match CH"
{
    Caption = 'Import Match CH';
    Direction = Import;
    FieldDelimiter = '<None>';
    FieldSeparator = ';';
    Format = VariableText;
    TextEncoding = WINDOWS;

    schema
    {
        textelement(root)
        {
            MinOccurs = Zero;
            tableelement(Integer; Integer)
            {
                AutoSave = false;
                XmlName = 'Import_Match_CH';
                SourceTableView = SORTING(Number) WHERE(Number = CONST(1));
                textelement(recordtyp)
                {
                    XmlName = 'A';
                }
                textelement(columnb)
                {
                    XmlName = 'B';
                }
                textelement(columnc)
                {
                    XmlName = 'C';
                }
                textelement(columnd)
                {
                    MinOccurs = Zero;
                    XmlName = 'D';
                }
                textelement(columne)
                {
                    MinOccurs = Zero;
                    XmlName = 'E';
                }
                textelement(columnf)
                {
                    MinOccurs = Zero;
                    XmlName = 'F';
                }
                textelement(columng)
                {
                    MinOccurs = Zero;
                    XmlName = 'G';
                }
                textelement(columnh)
                {
                    MinOccurs = Zero;
                    XmlName = 'H';
                }
                textelement(columni)
                {
                    MinOccurs = Zero;
                    XmlName = 'I';
                }
                textelement(columnj)
                {
                    MinOccurs = Zero;
                    XmlName = 'J';
                }
                textelement(columnk)
                {
                    MinOccurs = Zero;
                    XmlName = 'K';
                }
                textelement(columnl)
                {
                    MinOccurs = Zero;
                    XmlName = 'L';
                }
                textelement(columnm)
                {
                    MinOccurs = Zero;
                    XmlName = 'M';
                }
                textelement(columnn)
                {
                    MinOccurs = Zero;
                    XmlName = 'N';
                }
                textelement(columno)
                {
                    MinOccurs = Zero;
                    XmlName = 'O';
                }
                textelement(columnp)
                {
                    MinOccurs = Zero;
                    XmlName = 'P';
                }

                trigger OnAfterInsertRecord()
                begin
                    ProcesData;
                end;
            }
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    trigger OnPostXmlPort()
    var
        SwissPostCodeManagement: Codeunit "Swiss Post Code Management";
    begin
        InsertInvalidPostCodes;
        Window.Close;
        Commit;

        SwissPostCodeManagement.ShowInvalidPostCodes;
    end;

    trigger OnPreXmlPort()
    var
        CommunitynumberBfSBFSNR: Record "Community number BfS (BFSNR)";
        ZIPAltAreaDescription: Record "ZIP Alt. Area Description";
        PostCode: Record "Post Code";
        Street: Record "Street Directory M20";
    begin
        Window.Open(Text001);

        PostCode.SetFilter("Code serial number Post", '<>%1', '');
        if PostCode.FindSet then
            repeat
                TempInvalidPostCode := PostCode;
                TempInvalidPostCode.Insert;
                PostCode.Delete;
            until PostCode.Next = 0;

        ZIPAltAreaDescription.DeleteAll;
        Street.DeleteAll;

        CommunitynumberBfSBFSNR.ModifyAll(Inactive, true);
    end;

    var
        Text001: Label 'Daten werden importiert. Dies kann einige Minuten dauern.';
        TempInvalidPostCode: Record "Post Code" temporary;
        Window: Dialog;

    local procedure ProcesData()
    var
        PostCode: Record "Post Code";
        CommunitynumberBfSBFSNR: Record "Community number BfS (BFSNR)";
        ZIPAltAreaDescription: Record "ZIP Alt. Area Description";
        Street: Record "Street Directory M20";
        Day: Integer;
        Year: Integer;
        Month: Integer;
    begin
        // 00  NEW_HEA   Enthält das Versionsdatum und einen eindeutigen Zufallscode.
        // 01  NEW_PLZ1  Enthält alle für die Adressierung gültigen Postleitzahlen der Schweiz und des Fürstentums Liechtenstein.
        // 02  NEW_PLZ2  Enthält alternative Ortsbezeichnungen und Gebietsbezeichnungen zur jeweiligen Postleitzahl.
        // 03  NEW_COM   Enthält die politischen Gemeinden der Schweiz und des Fürstentums Liechtenstein. Diese Daten stammen aus der offiziellen Liste des Bundesamtes für Statistik (CommunitynumberBfSBFSNR).
        // 04  NEW_STR   Enthält alle Strassenbezeichnungen aller Ortschaften der Schweiz und des Fürstentums Liechtenstein.
        // 05  NEW_STRA  Enthält alternative oder fremdsprachige Strassenbezeichnungen.
        // 06  NEW_GEB   Enthält Hausnummer und Hauskey.
        // 07  NEW_GEBA  Enthält alternative Hausbezeichnung und alternativen Hauskey.
        // 08  NEW_BOT_B Enthält Boteninformationen auf Stufe Hausnummer (Briefzustellung).

        with Integer do begin
            case RecordTyp of
                '00':  // 00  NEW_HEA   Enthält das Versionsdatum und einen eindeutigen Zufallscode
                    begin
                    end;
                '01':  // 01  NEW_PLZ1  Enthält alle für die Adressierung gültigen Postleitzahlen der Schweiz und des Fürstentums Liechtenstein
                    begin
                        Clear(PostCode);
                        PostCode.Init;
                        PostCode."Code serial number Post" := ColumnB;
                        PostCode."Community number BfS" := ColumnC;
                        // ColumnD not used (PLZ_TYP Postleitzahl-Typ)
                        PostCode.Code := ColumnE;
                        PostCode."Additional number ZIP" := ColumnF;
                        //ColumnG not used (GPLZ Grundpostleitzahl)
                        PostCode."Place name (18 characters)" := ColumnH;
                        PostCode.Validate(City, ColumnI);
                        PostCode.County := ColumnJ;
                        PostCode."Language Code" := ColumnK;
                        if ColumnL <> '' then
                            PostCode."Language code, derogation" := ColumnL;
                        PostCode."Costlier by" := ColumnM;
                        if ColumnN <> '' then begin
                            Evaluate(Day, CopyStr(ColumnN, 7, 2));
                            Evaluate(Month, CopyStr(ColumnN, 5, 2));
                            Evaluate(Year, CopyStr(ColumnN, 1, 4));
                            PostCode."Valid from" := DMY2Date(Day, Month, Year);
                        end;
                        //ColumnO not used (PLZ_BRIEFZUST Postleitzahl der Zustellstelle)
                        //ColumnP not used (PLZ_COFF)

                        PostCode.Insert;

                        if TempInvalidPostCode.Get(PostCode.Code, PostCode.City) then
                            TempInvalidPostCode.Delete;
                    end;
                '02':  // 02  NEW_PLZ2  Enthält alternative Ortsbezeichnungen und Gebietsbezeichnungen zur jeweiligen Postleitzahl
                    begin
                        Clear(ZIPAltAreaDescription);
                        ZIPAltAreaDescription.Init;
                        ZIPAltAreaDescription."Code serial number Post" := ColumnB;
                        Evaluate(ZIPAltAreaDescription."Sequence number", Format(ColumnC));
                        ZIPAltAreaDescription."Designation Type" := ColumnD;
                        ZIPAltAreaDescription.Languagecode := ColumnE;
                        ZIPAltAreaDescription."Name, 18 digits" := ColumnF;
                        ZIPAltAreaDescription."Name, 27 digits" := ColumnG;
                        ZIPAltAreaDescription.Insert;
                    end;
                '03':  // 03  NEW_COM   Enthält die politischen Gemeinden der Schweiz und des Fürstentums Liechtenstein. Diese Daten stammen aus der offiziellen Liste des Bundesamtes für Statistik (CommunitynumberBfSBFSNR)
                    begin
                        Clear(CommunitynumberBfSBFSNR);
                        if CommunitynumberBfSBFSNR.Get(ColumnB) then begin
                            CommunitynumberBfSBFSNR.Inactive := false;
                            CommunitynumberBfSBFSNR.Modify;
                        end else begin
                            CommunitynumberBfSBFSNR.Init;
                            CommunitynumberBfSBFSNR."No." := ColumnB;
                            CommunitynumberBfSBFSNR.Name := ColumnC;
                            CommunitynumberBfSBFSNR.County := ColumnD;
                            CommunitynumberBfSBFSNR."Agglomeration number" := ColumnE;
                            CommunitynumberBfSBFSNR.Inactive := false;
                            CommunitynumberBfSBFSNR.Insert;
                        end;
                    end;
                '04':  // 04  NEW_STR   Enthält alle Strassenbezeichnungen aller Ortschaften der Schweiz und des Fürstentums Liechtenstein
                    begin
                        Clear(Street);
                        Street.Init;
                        Evaluate(Street."Street No.", ColumnB);
                        Street."Base Post Code" := ColumnC; //Base Post Code <> Code serial number
                        Street."Name of Street" := ColumnD; //TODO AA
                        //ColumnE not used (STRBEZL Ausgeschriebene Strassenbezeichnung)
                        //ColumnF not used (STRBEZ2K Abgekürzte umgestellte Strassenbezeichnung)
                        //ColumnG not used (STRBEZ2L Umgestellte Strassenbezeichnung)
                        Evaluate(Street."Type of Street", ColumnH);
                        Evaluate(Street."Language Code", ColumnI);
                        //ColumnJ not used (STRBEZ_COFF)
                        //ColumnK not used (STR_GANZFACH)
                        //ColumnL not used (STR_FACH_ONRP)
                        Clear(PostCode);
                        //TODO Bei der Tabelle Street Directory ist der "Base Post Code" von Lausanne '1000'. 
                        //Der "Code serial number Post" von Lausanne in der PLZ-Tabelle ist '132'. Wieso die Dateninkonsistenz?
                        PostCode.SetRange("Code serial number Post", Street."Base Post Code");
                        if PostCode.FindFirst then
                            Street."Post Code to address" := PostCode.Code + PostCode."Additional number ZIP";
                        Street.Insert;
                    end;
                '05':  // 05  NEW_STRA  Enthält alternative oder fremdsprachige Strassenbezeichnungen
                    begin
                    end;
                '06':  // 06  NEW_GEB   Enthält Hausnummer und Hauskey
                    begin
                    end;
                '07':  // 07  NEW_GEBA  Enthält alternative Hausbezeichnung und alternativen Hauskey
                    begin
                    end;
                '08':  // 08  NEW_BOT_B Enthält Boteninformationen auf Stufe Hausnummer (Briefzustellung)
                    begin
                    end;
            end;
        end;
    end;

    local procedure InsertInvalidPostCodes()
    var
        PostCode: Record "Post Code";
        InvalidPostcode: Record "Invalid Postcode";
    begin
        if InvalidPostcode.FindSet then
            repeat
                //AA++
                clear(PostCode);
                PostCode.SetRange(code, InvalidPostcode.Code);
                PostCode.SetRange(City, InvalidPostcode.City);
                IF PostCode.FindFirst() then
                    //if PostCode.Get(InvalidPostcode.Code, InvalidPostcode.City, InvalidPostcode."City Code ID") then
                    //AA--
                    InvalidPostcode.Delete;
            until InvalidPostcode.Next = 0;

        if TempInvalidPostCode.FindSet then
            repeat
                InvalidPostcode.Code := TempInvalidPostCode.Code;
                InvalidPostcode.City := TempInvalidPostCode.City;
                //XXX City Code ID von unitop. Pürfen ob noch vorhanden InvalidPostcode."City Code ID" := TempInvalidPostCode."City Code ID";
                InvalidPostcode."Code serial number Post" := TempInvalidPostCode."Code serial number Post";
                InvalidPostcode.Insert;
            until TempInvalidPostCode.Next = 0;
    end;

    procedure Ansi2Ascii(_String: Text[250]): Text[250]
    begin
        EXIT(CONVERTSTR(_String, 'Ã³ÚÔõÓÕþÛÙÞ´¯ý’µã¶÷ž¹¨ Íœ°úÏÎâßÝ¾·±Ð¬‡Š«Œ‹“”‘–—¤•ËÈÊš›™',
                                'ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®ÁÂÀÊËÈÍÎÏÌÓßÔÒÚÛÙ'));
    end;

    procedure Ascii2Ansi(_String: Text[1024]): Text[1024]
    begin
        EXIT(CONVERTSTR(_String, 'ÇüéâäàåçêëèïîìÄÅÉæÆôöòûùÿÖÜø£Ø×ƒáíóúñÑªº¿®ÁÂÀÊËÈÍÎÏÌÓßÔÒÚÛÙ',
                                'Ã³ÚÔõÓÕþÛÙÞ´¯ý’µã¶÷ž¹¨ Íœ°úÏÎâßÝ¾·±Ð¬‡Š«Œ‹“”‘–—¤•ËÈÊš›™'));
    end;
}

