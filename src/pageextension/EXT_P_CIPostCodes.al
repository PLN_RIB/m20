pageextension 72200 "P_EXT_CIPostCodes" extends "Post Codes"
{
    //TODO action hinzufügen, standart ausblenden
    actions
    {
        modify("Import Post Codes")
        {
            Visible = false;
        }
        addlast("F&unctions")
        {

            action(Action72100)
            {
                ApplicationArea = Basic, Suite;
                Caption = 'Import Post Codes';
                Image = Import;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                RunObject = report 1017;
                ToolTip = 'Update the postal code directory with information from the Swiss Post website. All postal codes in the range 1000 to 9999 will be deleted before import. International postal codes, which include a country/region code, such as DE-60000, are retained.';
            }

        }
    }
}





