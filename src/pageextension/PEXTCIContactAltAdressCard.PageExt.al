pageextension 72210 "P_EXT_CIContactAltAdressCard" extends "Contact Alt. Address Card"
{
    layout
    {
        modify("Address 2")
        {
            trigger OnLookup(var Text: Text): Boolean
            var
                "***M20***": Integer;
                StreetDirectory: Record "M20 Street Directory";
            begin
                // +M20
                Commit;
                StreetDirectory.SelectStreet("Post Code", DATABASE::Contact, Rec.RecordId, 1, "Country/Region Code", "Address 2", CurrPage.Editable);
                // -M20
            end;
        }
    }
}
