pageextension 72201 "M20 P_EXT_CICompanyDetails" extends "Company Details"
{
    layout
    {
        modify(Address)
        {

            trigger OnAssistEdit()
            var
                StreetDirectoryM20: Record "M20 Street Directory";
            begin
                // +M20
                Commit;
                StreetDirectoryM20.SelectStreet("Post Code", DATABASE::Contact, Rec.RecordId, 1, "Country/Region Code", Address, CurrPage.Editable);
                // -M20
            end;
        }
    }
}
