pageextension 1001 "M20 P_EXT_CIContactCard" extends "Contact Card"
{
    layout
    {

        modify("Address 2")
        {
            trigger OnLookup(var Text: Text): Boolean
            var
                "***M20***": Integer;
                StreetDirectory: Record "M20 Street Directory";
            begin
                // +M20
                Commit;
                if StreetDirectory.SelectStreet("Post Code", DATABASE::Contact, Rec.RecordId, 1, "Country/Region Code", "Address 2", CurrPage.Editable) then
                    CurrPage.Update;
                // -M20
            end;
        }
    }
}
