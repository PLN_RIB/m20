tableextension 7200 "T_EXT_CIPostCode" extends "Post Code"
{
    fields
    {

        //TODO neue felder ab hier 
        field(72200; "Language Code"; Code[10])
        {
            Caption = 'Language Code';
            DataClassification = ToBeClassified;
            Description = 'M20';
            TableRelation = Language;
        }
        field(72201; "Code serial number Post"; Code[10])
        {
            Caption = 'Code serial number Post';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72202; "Additional number ZIP"; Code[10])
        {
            Caption = 'Additional number ZIP';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72203; "Place name (18 characters)"; Text[30])
        {
            Caption = 'Place name (18 characters)';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72204; "Language code, derogation"; Code[10])
        {
            Caption = 'Language code, derogation';
            DataClassification = ToBeClassified;
            Description = 'M20';
            TableRelation = Language;
        }
        field(72205; "Sort file available"; Boolean)
        {
            Caption = 'Sort file available';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72206; "Costlier by"; Code[20])
        {
            Caption = 'Costlier by';
            DataClassification = ToBeClassified;
            Description = 'M20';
            TableRelation = "Post Code";
        }
        field(72207; "Community number BfS"; Code[10])
        {
            Caption = 'Community number BfS';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72208; "Valid from"; Date)
        {
            Caption = 'Valid from';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72209; "Update Sequence"; Integer)
        {
            Caption = 'Update Sequence';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72210; "Mutation status"; Option)
        {
            Caption = 'Mutation status';
            DataClassification = ToBeClassified;
            Description = 'M20';
            OptionCaption = 'New,Change,Out of service,Back in operation';
            OptionMembers = New,Change,"Out of service","Back in operation";
        }
        field(72211; "Replacement ZIP PO box"; Code[10])
        {
            Caption = 'Replacement ZIP PO box';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72212; "ORNP Repl. ZIP PO box"; Code[10])
        {
            Caption = 'ORNP Repl. ZIP PO box';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72213; "Replacement postcode domicile"; Code[10])
        {
            Caption = 'Replacement postcode domicile';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72214; "ORNP Repl. postcode domicile"; Code[10])
        {
            Caption = 'ORNP Repl. postcode domicile';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(72215; "Region Name Type"; Code[1])
        {
            Caption = 'Region Name Type';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
    }
}

