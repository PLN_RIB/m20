table 72204 "Additional Fields"
{
    // Table 5050:
    //   Type = 1: Adress Post Code
    //   Type = 2: ZIP Post Code
    // Table 36,5900
    //   Type = 1: Sell-to
    //   Type = 2: Ship-to
    //   Type = 3: Bill-to
    // Table 38
    //   Type = 1: Buy-from
    //   Type = 2: Ship-to
    //   Type = 3: Pay-to

    Caption = 'Additional Fields';

    fields
    {
        field(1; "Table ID"; Integer)
        {
            Caption = 'Table ID';
            DataClassification = ToBeClassified;
        }
        field(2; RecordID; RecordID)
        {
            Caption = 'RecordID';
            DataClassification = ToBeClassified;
        }
        field(3; Type; Integer)
        {
            Caption = 'Type';
            DataClassification = ToBeClassified;
            InitValue = 1;
        }
        field(10; "Additional number ZIP"; Code[10])
        {
            Caption = 'Additional number ZIP';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(11; "Community number BfS (BFSNR)"; Code[10])
        {
            Caption = 'Community number BfS (BFSNR)';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(12; "Code serial number Post"; Code[10])
        {
            Caption = 'Code serial number Post';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(13; "Invalid serial number post"; Boolean)
        {
            CalcFormula = - Exist("Post Code" WHERE("Code serial number Post" = FIELD("Code serial number Post")));
            Caption = 'Ungültige Ordnungsnummer Post';
            Description = 'M20';
            Editable = false;
            FieldClass = FlowField;
        }
        field(14; "Name of Street"; Text[30])
        {
            Caption = 'Name of Street';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
        field(15; "House No."; Text[10])
        {
            Caption = 'House No.';
            DataClassification = ToBeClassified;
            Description = 'M20';
        }
    }

    keys
    {
        key(Key1; "Table ID", RecordID, Type)
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

