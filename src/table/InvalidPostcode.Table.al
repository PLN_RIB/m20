table 72202 "Invalid Postcode"
{
    Caption = 'Invalid Postcode';

    fields
    {
        field(1; "Code"; Code[20])
        {
            Caption = 'Code';
            DataClassification = ToBeClassified;
            NotBlank = true;
        }
        field(2; City; Text[30])
        {
            Caption = 'City';
            DataClassification = ToBeClassified;
            NotBlank = true;
        }
        field(3; "City Code ID"; Code[10])
        {
            Caption = 'City Code ID';
            DataClassification = ToBeClassified;
        }
        field(10; "Code serial number Post"; Code[10])
        {
            Caption = 'Code serial number Post';
            DataClassification = ToBeClassified;
        }
        field(20; "No. of Contacts"; Integer)
        {
            CalcFormula = Count(Contact WHERE("Post Code" = FIELD(Code)));
            Caption = 'No. of Contacts';
            Editable = false;
            FieldClass = FlowField;
        }
        field(21; "No. of alt. Addresses"; Integer)
        {
            CalcFormula = Count("Contact Alt. Address" WHERE("Post Code" = FIELD(Code)));
            Caption = 'Anzahl alternative Adressen';
            Editable = false;
            FieldClass = FlowField;
        }
    }

    keys
    {
        key(Key1; "Code", City, "City Code ID")
        {
            Clustered = true;
        }
    }
}

