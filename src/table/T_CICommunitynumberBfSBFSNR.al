table 72201 "Community number BfS (BFSNR)"
{
    Caption = 'Community number BfS (BFSNR)';
    DrillDownPageID = "Community number BfS (BFSNR)";
    LookupPageID = "Community number BfS (BFSNR)";

    fields
    {
        field(1; "No."; Code[10])
        {
            Caption = 'No.';
        }
        field(5; Name; Text[30])
        {
            Caption = 'Name';
        }
        field(10; County; Code[10])
        {
            Caption = 'County';
        }
        field(15; "Agglomeration number"; Code[10])
        {
            Caption = 'Agglomeration number';
        }
        field(16; Inactive; Boolean)
        {
            Caption = 'Inactive';
            DataClassification = ToBeClassified;
        }
    }

    keys
    {
        key(Key1; "No.")
        {
            Clustered = true;
        }
    }

    fieldgroups
    {
    }
}

