table 72203 "ZIP Alt. Area Description"
{
    Caption = 'ZIP alternative- und area designation';

    fields
    {
        field(1; "Code serial number Post"; Code[20])
        {
            Caption = 'Code serial number Post';
        }
        field(5; "Sequence number"; Integer)
        {
            Caption = 'Sequence number';
        }
        field(10; "Designation Type"; Code[1])
        {
            Caption = 'Designation Type';
        }
        field(15; Languagecode; Code[10])
        {
            Caption = 'Languagecode';
            TableRelation = Language.Code;
        }
        field(20; "Name, 18 digits"; Text[30])
        {
            Caption = 'Name, 18 digits';
        }
        field(25; "Name, 27 digits"; Text[30])
        {
            Caption = 'Name, 27 digits';
        }
        field(30; "Update Sequence"; Integer)
        {
            Caption = 'Update Sequence';
        }
        field(35; "Mutation status"; Option)
        {
            Caption = 'Mutation status';
            OptionCaption = 'New,Change,Out of service,Back in operation';
            OptionMembers = New,Change,"Out of service","Back in operation";
        }
        field(40; "My new ZIP"; Code[20])
        {
            Caption = 'My new ZIP';
            TableRelation = "Post Code";
        }
        field(45; "MY ONRP new ZIP PO box"; Code[20])
        {
            Caption = 'MY ONRP new ZIP PO box';
            TableRelation = "Post Code";
        }
        field(50; "ZIP insinuation new"; Code[20])
        {
            Caption = 'ZIP insinuation new';
            TableRelation = "Post Code";
        }
        field(55; "ONRP new ZIP insinuation"; Code[20])
        {
            Caption = 'ONRP new ZIP insinuation';
            TableRelation = "Post Code";
        }
    }

    keys
    {
        key(Key1; "Code serial number Post", "Sequence number")
        {
            Clustered = true;
        }
        key(Key2; "Name, 27 digits")
        {
        }
    }

    fieldgroups
    {
    }
}

