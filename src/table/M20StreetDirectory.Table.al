table 72200 "M20 Street Directory"
{
    Caption = 'Street Directory';
    LookupPageID = "Street Directory M20";


    fields
    {
        field(1; "Street No."; Integer)
        {
            Caption = 'Street No.';
        }
        field(10; "Base Post Code"; Code[20])
        {
            Caption = 'Base Post Code';
            //TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            //The property 'ValidateTableRelation' can only be set if the property 'TableRelation' is set
            //ValidateTableRelation = false;

            trigger OnLookup()
            begin
                Clear(PostCode);
                PostCode.SetRange(Code, CopyStr("Post Code to address", 1, 4));

                if PAGE.RunModal(0, PostCode) = ACTION::LookupOK then;
            end;
        }
        field(20; "Language Code"; Integer)
        {
            Caption = 'Language Code';
            Description = 'Sprachcode (nur bei 2-sprachigen Orten wie Biel/Bienne 1=deutsch, 2=französisch,3=italienisch)';
        }
        field(30; "Name of Street (Uppercase)"; Code[25])
        {
            Caption = 'Name of Street (Uppercase)';
        }
        field(40; "Post Code to address"; Code[6])
        {
            Caption = 'Post Code to address';
        }
        field(50; "Split Code"; Integer)
        {
            Caption = 'Split Code';
            Description = '1=gerade und ungerade Hausnummern, 2=gerade, 3=ungerade';
        }
        field(60; "House No. from"; Integer)
        {
            Caption = 'House No. from';
        }
        field(70; "House No. from (alpha)"; Text[2])
        {
            Caption = 'House No. from (alpha)';
        }
        field(80; "House No. to"; Integer)
        {
            Caption = 'House No. to';
        }
        field(90; "House No. to (alpha)"; Text[2])
        {
            Caption = 'House No. to (alpha)';
        }
        field(100; "Name of Street"; Text[25])
        {
            Caption = 'Name of Street';
        }
        field(110; "Street Root"; Code[10])
        {
            Caption = 'Street Root';
        }
        field(120; "Type of Street"; Integer)
        {
            Caption = 'Type of Street';
        }
        field(130; "Preposition Code"; Integer)
        {
            Caption = 'Preposition Code';
        }
    }

    keys
    {
        key(Key1; "Street No.", "Split Code")
        {
            Clustered = true;
        }
        key(Key2; "Post Code to address", "Name of Street")
        {
        }
    }

    fieldgroups
    {
    }

    var
        StreetDirectory: Record "M20 Street Directory";
        PostCode: Record "Post Code";
        Text002: Label 'No Street Directory for Country %1 found.';

    [Scope('OnPrem')]
    procedure LookupAddress(var NameOfStreet: Text[50]; var PostCode: Code[20]; var PostCodeAdd: Code[10]; CountryCode: Code[10])
    var
        PostCodeAndAdd: Code[6];
    begin
        if CountryCode = '' then begin
            Clear(StreetDirectory);
            if PostCode <> '' then begin
                if PostCodeAdd = '' then
                    PostCodeAndAdd := CopyStr(PostCode, 1, 4) + '*' // 'OO'
                else
                    PostCodeAndAdd := CopyStr(PostCode, 1, 4) + CopyStr(PostCodeAdd, 1, 2);

                StreetDirectory.SetCurrentKey("Post Code to address", "Name of Street");
                StreetDirectory.SetFilter("Post Code to address", PostCodeAndAdd); //SetRange
            end;

            if PAGE.RunModal(PAGE::"Street Directory M20", StreetDirectory) = ACTION::LookupOK then begin
                NameOfStreet := StreetDirectory."Name of Street";
                PostCode := CopyStr(StreetDirectory."Post Code to address", 1, 4);
                PostCodeAdd := CopyStr(StreetDirectory."Post Code to address", 5, 2);
            end;
        end else
            Message(Text002, CountryCode);
    end;

    [Scope('OnPrem')]
    procedure CreateAdress(var LocAddress: Text[30]; var NameOfStreet: Text[25]; var HouseNo: Text[10]; ZIPCode: Code[20])
    var
        KommaPos: Integer;
        StreetNamePart1: Text[30];
        StreetNamePart2: Text[30];
    begin
        KommaPos := StrPos(NameOfStreet, ',');

        if KommaPos <> 0 then begin
            StreetNamePart2 := CopyStr(NameOfStreet, 1, KommaPos - 1);
            StreetNamePart1 := CopyStr(NameOfStreet, KommaPos + 1, StrLen(NameOfStreet));
            StreetNamePart1 := DelChr(StreetNamePart1, '<', ' ');
            LocAddress := StrSubstNo('%1 %2', StreetNamePart1, StreetNamePart2);
        end else
            LocAddress := NameOfStreet;

        if HouseNo <> '' then
            LocAddress := StrSubstNo('%1 %2', LocAddress, HouseNo);
    end;

    [Scope('OnPrem')]
    procedure SelectStreet(PostCode: Code[20]; TableID: Integer; RecordID: RecordID; Type: Integer; CountryRegion: Code[10]; var Address: Text[30]; EditablePage: Boolean) FormUpdate: Boolean
    var
        AdditionalFields: Record "Additional Fields";
        StreetName: Text;
        HouseNo: Text;
        StreetSelect: Page "Streetselect Address";
        SwissPostCodeManagement: Codeunit "M20 Swiss Post Code Management";
    begin
        Clear(AdditionalFields);
        SwissPostCodeManagement.GetAdditionalFields(TableID, RecordID, Type, AdditionalFields);

        FormUpdate := false;
        Clear(StreetSelect);
        StreetSelect.SetVariables(PostCode, AdditionalFields."Additional number ZIP", CountryRegion, AdditionalFields."Name of Street", AdditionalFields."House No.", Address);
        StreetSelect.LookupMode(true);
        if StreetSelect.RunModal = ACTION::LookupOK then begin
            StreetSelect.GetVariables(StreetName, HouseNo, Address);
            AdditionalFields."Name of Street" := StreetName;
            AdditionalFields."House No." := HouseNo;
            if not AdditionalFields.Insert(true) then
                AdditionalFields.Modify(true);
            FormUpdate := true;
        end;
    end;
}

