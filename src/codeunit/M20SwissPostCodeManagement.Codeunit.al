codeunit 72200 "M20 Swiss Post Code Management"
{

    trigger OnRun()
    begin
    end;

    var
        Text001: Label '%1 Kontakte haben ev. eine ungültige PLZ. Wollen Sie die Kontakte bearbeiten?';

    [EventSubscriber(ObjectType::Table, 14, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure Location_OnAfterValidateEvent_City(var Rec: Record Location; var xRec: Record Location; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Location, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 14, 'OnAfterValidateEvent', 'Post code', false, false)]
    local procedure Location_OnAfterValidateEvent_PostCode(var Rec: Record Location; var xRec: Record Location; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Location, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 14, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Location_OnAfterDeleteEvent(var Rec: Record Location; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Location, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 18, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure Customer_OnAfterValidateEvent_City(var Rec: Record Customer; var xRec: Record Customer; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Customer, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 18, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure Customer_OnAfterValidateEvent_PostCode(var Rec: Record Customer; var xRec: Record Customer; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Customer, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 18, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Customer_OnAfterDeleteEvent(var Rec: Record Customer; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Customer, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 23, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure Vendor_OnAfterValidateEvent_City(var Rec: Record Vendor; var xRec: Record Vendor; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Vendor, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 23, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure Vendor_OnAfterValidateEvent_PostCode(var Rec: Record Vendor; var xRec: Record Vendor; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Vendor, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 23, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Vendor_OnAfterDeleteEvent(var Rec: Record Vendor; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Vendor, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Bill-to City', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_BillToCity(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Sales Header", Rec.RecordId, 3, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Bill-to Post Code', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_BillToPostCode(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Sales Header", Rec.RecordId, 3, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Ship-to City', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_ShipToCity(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Sales Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Ship-to Post Code', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_ShipToPostCode(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Sales Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Sell-to City', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_SellToCity(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Sales Header", Rec.RecordId, 1, Rec."Sell-to City", Rec."Sell-to Post Code", Rec."Sell-to County", Rec."Sell-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterValidateEvent', 'Sell-to Post Code', false, false)]
    local procedure SalesHeader_OnAfterValidateEvent_SellToPostCode(var Rec: Record "Sales Header"; var xRec: Record "Sales Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Sales Header", Rec.RecordId, 1, Rec."Sell-to City", Rec."Sell-to Post Code", Rec."Sell-to County", Rec."Sell-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 36, 'OnAfterDeleteEvent', '', false, false)]
    local procedure SalesHeader_OnAfterDeleteEvent(var Rec: Record "Sales Header"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Sales Header", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Pay-to City', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_PayToCity(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Purchase Header", Rec.RecordId, 3, Rec."Pay-to City", Rec."Pay-to Post Code", Rec."Pay-to County", Rec."Pay-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Pay-to Post Code', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_PayToPostCode(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Purchase Header", Rec.RecordId, 3, Rec."Pay-to City", Rec."Pay-to Post Code", Rec."Pay-to County", Rec."Pay-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Ship-to City', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_ShipToCity(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Purchase Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Ship-to Post Code', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_ShipToPostCode(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Purchase Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Buy-from City', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_BuyFromCity(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Purchase Header", Rec.RecordId, 1, Rec."Buy-from City", Rec."Buy-from Post Code", Rec."Buy-from County", Rec."Buy-from Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterValidateEvent', 'Buy-from Post Code', false, false)]
    local procedure PurchaseHeader_OnAfterValidateEvent_BuyFromPostCode(var Rec: Record "Purchase Header"; var xRec: Record "Purchase Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Purchase Header", Rec.RecordId, 1, Rec."Buy-from City", Rec."Buy-from Post Code", Rec."Buy-from County", Rec."Buy-from Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 38, 'OnAfterDeleteEvent', '', false, false)]
    local procedure PurchaseHeader_OnAfterDeleteEvent(var Rec: Record "Purchase Header"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Purchase Header", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 79, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure CompanyInformation_OnAfterValidateEvent_City(var Rec: Record "Company Information"; var xRec: Record "Company Information"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Company Information", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 79, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure CompanyInformation_OnAfterValidateEvent_PostCode(var Rec: Record "Company Information"; var xRec: Record "Company Information"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Company Information", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 79, 'OnAfterDeleteEvent', '', false, false)]
    local procedure CompanyInformation_OnAfterDeleteEvent(var Rec: Record "Company Information"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Company Information", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 156, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure Resource_OnAfterValidateEvent_City(var Rec: Record Resource; var xRec: Record Resource; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Resource, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 156, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure Resource_OnAfterValidateEvent_PostCode(var Rec: Record Resource; var xRec: Record Resource; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Resource, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 156, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Resource_OnAfterDeleteEvent(var Rec: Record Resource; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Resource, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 167, 'OnAfterValidateEvent', 'Bill-to City', false, false)]
    local procedure Job_OnAfterValidateEvent_BillToCity(var Rec: Record Job; var xRec: Record Job; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Purchase Header", Rec.RecordId, 1, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 167, 'OnAfterValidateEvent', 'Bill-to Post Code', false, false)]
    local procedure Job_OnAfterValidateEvent_BillToPostCode(var Rec: Record Job; var xRec: Record Job; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Purchase Header", Rec.RecordId, 1, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 167, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Job_OnAfterDeleteEvent(var Rec: Record Job; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Job, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 222, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure ShiptoAddress_OnAfterValidateEvent_City(var Rec: Record "Ship-to Address"; var xRec: Record "Ship-to Address"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Ship-to Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 222, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure ShiptoAddress_OnAfterValidateEvent_PostCode(var Rec: Record "Ship-to Address"; var xRec: Record "Ship-to Address"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Ship-to Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 222, 'OnAfterDeleteEvent', '', false, false)]
    local procedure ShiptoAddress_OnAfterDeleteEvent(var Rec: Record "Ship-to Address"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Ship-to Address", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 224, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure OrderAddress_OnAfterValidateEvent_City(var Rec: Record "Order Address"; var xRec: Record "Order Address"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Order Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 224, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure OrderAddress_OnAfterValidateEvent_PostCode(var Rec: Record "Order Address"; var xRec: Record "Order Address"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Order Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 224, 'OnAfterDeleteEvent', '', false, false)]
    local procedure OrderAddress_OnAfterDeleteEvent(var Rec: Record "Order Address"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Order Address", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 270, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure BankAccount_OnAfterValidateEvent_City(var Rec: Record "Bank Account"; var xRec: Record "Bank Account"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 270, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure BankAccount_OnAfterValidateEvent_PostCode(var Rec: Record "Bank Account"; var xRec: Record "Bank Account"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 270, 'OnAfterDeleteEvent', '', false, false)]
    local procedure BankAccount_OnAfterDeleteEvent(var Rec: Record "Bank Account"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Bank Account", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 287, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure CustomerBankAccount_OnAfterValidateEvent_City(var Rec: Record "Customer Bank Account"; var xRec: Record "Customer Bank Account"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Customer Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 287, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure CustomerBankAccount_OnAfterValidateEvent_PostCode(var Rec: Record "Customer Bank Account"; var xRec: Record "Customer Bank Account"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Customer Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 287, 'OnAfterDeleteEvent', '', false, false)]
    local procedure CustomerBankAccount_OnAfterDeleteEvent(var Rec: Record "Customer Bank Account"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Customer Bank Account", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 288, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure VendorBankAccount_OnAfterValidateEvent_City(var Rec: Record "Vendor Bank Account"; var xRec: Record "Vendor Bank Account"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Vendor Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 288, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure VendorBankAccount_OnAfterValidateEvent_PostCode(var Rec: Record "Vendor Bank Account"; var xRec: Record "Vendor Bank Account"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Vendor Bank Account", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 288, 'OnAfterDeleteEvent', '', false, false)]
    local procedure VendorBankAccount_OnAfterDeleteEvent(var Rec: Record "Vendor Bank Account"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Vendor Bank Account", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 295, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure ReminderHeader_OnAfterValidateEvent_City(var Rec: Record "Reminder Header"; var xRec: Record "Reminder Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Reminder Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 295, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure ReminderHeader_OnAfterValidateEvent_PostCode(var Rec: Record "Reminder Header"; var xRec: Record "Reminder Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Reminder Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 295, 'OnAfterDeleteEvent', '', false, false)]
    local procedure ReminderHeader_OnAfterDeleteEvent(var Rec: Record "Reminder Header"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Reminder Header", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 302, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure FinanceChargeMemoHeader_OnAfterValidateEvent_City(var Rec: Record "Finance Charge Memo Header"; var xRec: Record "Finance Charge Memo Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Finance Charge Memo Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 302, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure FinanceChargeMemoHeader_OnAfterValidateEvent_PostCode(var Rec: Record "Finance Charge Memo Header"; var xRec: Record "Finance Charge Memo Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Finance Charge Memo Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 302, 'OnAfterDeleteEvent', '', false, false)]
    local procedure FinanceChargeMemoHeader_OnAfterDeleteEvent(var Rec: Record "Finance Charge Memo Header"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Finance Charge Memo Header", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 5050, 'OnBeforeValidateEvent', 'City', false, false)]
    local procedure Contact_OnBeforeValidateEvent_City(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Contact, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5050, 'OnBeforeValidateEvent', 'Post Code', false, false)]
    local procedure Contact_OnBeforeValidateEvent_PostCode(var Rec: Record Contact; var xRec: Record Contact; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Contact, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5050, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Contact_OnAfterDeleteEvent(var Rec: Record Contact; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Contact, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 5051, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure ContactAltAddress_OnAfterValidateEvent_City(var Rec: Record "Contact Alt. Address"; var xRec: Record "Contact Alt. Address"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Contact Alt. Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5051, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure ContactAltAddress_OnAfterValidateEvent_PostCode(var Rec: Record "Contact Alt. Address"; var xRec: Record "Contact Alt. Address"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Contact Alt. Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5051, 'OnAfterDeleteEvent', '', false, false)]
    local procedure ContactAltAddress_OnAfterDeleteEvent(var Rec: Record "Contact Alt. Address"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Contact Alt. Address", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 5200, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure Employee_OnAfterValidateEvent_City(var Rec: Record Employee; var xRec: Record Employee; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::Employee, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5200, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure Employee_OnAfterValidateEvent_PostCode(var Rec: Record Employee; var xRec: Record Employee; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::Employee, Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5200, 'OnAfterDeleteEvent', '', false, false)]
    local procedure Employee_OnAfterDeleteEvent(var Rec: Record Employee; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::Employee, Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 5201, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure AlternativeAddress_OnAfterValidateEvent_City(var Rec: Record "Alternative Address"; var xRec: Record "Alternative Address"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Alternative Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5201, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure AlternativeAddress_OnAfterValidateEvent_PostCode(var Rec: Record "Alternative Address"; var xRec: Record "Alternative Address"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Alternative Address", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5201, 'OnAfterDeleteEvent', '', false, false)]
    local procedure AlternativeAddress_OnAfterDeleteEvent(var Rec: Record "Alternative Address"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Alternative Address", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'Bill-to City', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_BillToCity(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Service Header", Rec.RecordId, 3, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'Bill-to Post Code', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_BillToPostCode(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Service Header", Rec.RecordId, 3, Rec."Bill-to City", Rec."Bill-to Post Code", Rec."Bill-to County", Rec."Bill-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'Ship-to City', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_ShipToCity(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Service Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'Ship-to Post Code', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_ShipToPostCode(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Service Header", Rec.RecordId, 2, Rec."Ship-to City", Rec."Ship-to Post Code", Rec."Ship-to County", Rec."Ship-to Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'City', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_City(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidateCity(DATABASE::"Service Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterValidateEvent', 'Post Code', false, false)]
    local procedure ServiceHeader_OnAfterValidateEvent_PostCode(var Rec: Record "Service Header"; var xRec: Record "Service Header"; CurrFieldNo: Integer)
    begin
        ValidatePostCode(DATABASE::"Service Header", Rec.RecordId, 1, Rec.City, Rec."Post Code", Rec.County, Rec."Country/Region Code", (CurrFieldNo <> 0) and GuiAllowed);
    end;

    [EventSubscriber(ObjectType::Table, 5900, 'OnAfterDeleteEvent', '', false, false)]
    local procedure ServiceHeader_OnAfterDeleteEvent(var Rec: Record "Service Header"; RunTrigger: Boolean)
    begin
        DeleteAdditionalFields(DATABASE::"Service Header", Rec.RecordId, 1);
    end;

    [EventSubscriber(ObjectType::Table, 225, 'OnBeforeValidatePostCode', '', false, false)]
    local procedure PostCode_OnBeforeValidatePostCode(var CityTxt: Text[30]; var PostCode: Code[20]; var CountyTxt: Text[30]; var CountryCode: Code[10]; UseDialog: Boolean; var IsHandled: Boolean)
    begin
        IsHandled := true;
    end;

    local procedure DeleteAdditionalFields(TableID: Integer; RecordID: RecordID; Type: Integer)
    var
        AdditionalFields: Record "Additional Fields";
    begin
        if AdditionalFields.Get(TableID, RecordID, Type) then
            AdditionalFields.Delete;
    end;

    local procedure DelData(var City2: Text[30]; var PostCode: Code[20]; var CodeSerialNo: Code[10]; var County2: Text[30]; var AdditionalNo: Code[10]; var CommunityNo: Code[10]; var CountryRegionCode: Code[10])
    begin
        City2 := '';
        PostCode := '';
        CodeSerialNo := '';
        County2 := '';
        AdditionalNo := '';
        CommunityNo := '';
        CountryRegionCode := '';
    end;

    [Scope('OnPrem')]
    procedure GetAdditionalFields(TableID: Integer; RecordID: RecordID; Type: Integer; var AdditionalFields: Record "Additional Fields"): Code[10]
    begin
        if not AdditionalFields.Get(TableID, RecordID, Type) then begin
            AdditionalFields."Table ID" := TableID;
            AdditionalFields.RecordID := RecordID;
            AdditionalFields.Type := Type;
            AdditionalFields.Insert();
            Commit();
        end;
    end;

    [Scope('OnPrem')]
    procedure GetLanguage(ZIP: Code[20]; City: Text; Substitute: Boolean): Code[10]
    var
        PostCode: Record "Post Code";
    begin
        Clear(PostCode);
        PostCode.SetCurrentKey(City, Code);
        PostCode.SetRange(City, City);
        PostCode.SetRange(Code, ZIP);
        if PostCode.FindFirst then begin
            if Substitute then
                exit(PostCode."Language code, derogation")
            else
                exit(PostCode."Language Code");
        end else begin
            PostCode.SetRange(City);
            if PostCode.FindFirst then
                if Substitute then
                    exit(PostCode."Language code, derogation")
                else
                    exit(PostCode."Language Code");
        end;
    end;

    local procedure LookupTempTable(var City2: Text[30]; var PostCode: Code[20]; var CodeSerialNo: Code[10]; var County2: Text[30]; var AdditionalNo: Code[10]; var CommunityNo: Code[10]; var CountryRegionCode: Code[10]; UseDialog: Boolean)
    var
        PostCodeRec: Record "Post Code";
        PostCodeTemp: Record "Post Code" temporary;
        ZIPAltAreaDescription: Record "ZIP Alt. Area Description";
        LineNo: Integer;
        FilterString: Text[250];
    begin
        FilterString := StrSubstNo('@*%1*', City2);

        if (City2 = '') and (PostCode = '') then begin
            DelData(City2, PostCode, CodeSerialNo, County2, AdditionalNo, CommunityNo, CountryRegionCode);
            exit;
        end;

        if City2 <> '' then begin
            FilterString := City2;
            PostCodeRec.SetCurrentKey("Search City");
            if StrPos(FilterString, '*') = StrLen(FilterString) then
                PostCodeRec.SetFilter("Search City", FilterString)
            else
                PostCodeRec.SetRange("Search City", FilterString);
        end;

        if PostCode <> '' then
            if StrPos(PostCode, '*') = StrLen(PostCode) then
                PostCodeRec.SetFilter(Code, PostCode)
            else
                PostCodeRec.SetRange(Code, PostCode);

        if not PostCodeRec.FindFirst then
            PostCodeRec.SetRange(Code);

        if PostCodeRec.FindFirst then
            repeat
                PostCodeTemp.Init;
                LineNo += 10000;
                PostCodeTemp."Code serial number Post" := Format(LineNo);
                PostCodeTemp.Code := PostCodeRec.Code;
                PostCodeTemp."Additional number ZIP" := PostCodeRec."Additional number ZIP";
                PostCodeTemp.City := PostCodeRec.City;
                PostCodeTemp."ORNP Repl. postcode domicile" := PostCodeRec."Code serial number Post";
                PostCodeTemp.County := PostCodeRec.County;
                PostCodeTemp."Country/Region Code" := PostCodeRec."Country/Region Code";
                PostCodeTemp.Insert;
            until PostCodeRec.Next = 0;

        ZIPAltAreaDescription.SetCurrentKey("Name, 27 digits");
        ZIPAltAreaDescription.SetRange("Name, 27 digits", FilterString);
        if ZIPAltAreaDescription.FindFirst then
            repeat
                PostCodeTemp.Init;
                LineNo += 10000;
                PostCodeTemp."Code serial number Post" := Format(LineNo);
                PostCodeRec.SetRange("Code serial number Post", ZIPAltAreaDescription."Code serial number Post");
                if PostCodeRec.FindFirst then begin
                    PostCodeTemp.Code := PostCodeRec.Code;
                    PostCodeTemp."Additional number ZIP" := PostCodeRec."Additional number ZIP";
                    PostCodeTemp.City := ZIPAltAreaDescription."Name, 27 digits";
                    PostCodeTemp."ORNP Repl. postcode domicile" := ZIPAltAreaDescription."Code serial number Post";
                    PostCodeTemp."Region Name Type" := ZIPAltAreaDescription."Designation Type";
                    PostCodeTemp.County := PostCodeRec.County;
                    PostCodeTemp."Country/Region Code" := PostCodeRec."Country/Region Code";
                    PostCodeTemp.Insert;
                end;
            until ZIPAltAreaDescription.Next = 0;

        if PostCodeTemp.IsEmpty then
            exit;

        if UseDialog then
            if (PAGE.RunModal(PAGE::"Post Codes", PostCodeTemp, PostCodeTemp.City) = ACTION::LookupOK) then begin
                PostCode := PostCodeTemp.Code;
                if PostCodeTemp."Region Name Type" = '2' then begin
                    City2 := PostCodeTemp.City;
                    County2 := PostCodeTemp.County;
                    AdditionalNo := PostCodeTemp."Additional number ZIP";
                    CommunityNo := PostCodeTemp."Community number BfS";
                    CountryRegionCode := PostCodeTemp."Country/Region Code";
                end else begin
                    City2 := PostCodeRec.City;
                    County2 := PostCodeRec.County;
                    AdditionalNo := PostCodeTemp."Additional number ZIP";
                    CommunityNo := PostCodeTemp."Community number BfS";
                    CountryRegionCode := PostCodeTemp."Country/Region Code";
                end;

                CodeSerialNo := PostCodeTemp."ORNP Repl. postcode domicile";
            end;
    end;

    [Scope('OnPrem')]
    procedure ShowInvalidPostCodes()
    var
        InvalidPostcode: Record "Invalid Postcode";
        CountPostCodes: Integer;
    begin
        if InvalidPostcode.FindSet then begin
            repeat
                InvalidPostcode.CalcFields("No. of Contacts", "No. of alt. Addresses");
                if (InvalidPostcode."No. of Contacts" <> 0) or (InvalidPostcode."No. of alt. Addresses" <> 0) then
                    CountPostCodes += 1;
            until InvalidPostcode.Next = 0;

            if CountPostCodes <> 0 then
                if Confirm(Text001, true, CountPostCodes) then
                    PAGE.RunModal(PAGE::"Invalid Postcodes", InvalidPostcode);
        end;
    end;

    local procedure ValidateCity(TableID: Integer; RecordID: RecordID; Type: Integer; var City2: Text[30]; var PostCode: Code[10]; var County2: Text[30]; var CountryRegionCode: Code[10]; UseDialog: Boolean)
    var
        AdditionalFields: Record "Additional Fields";
    begin
        if not GuiAllowed then
            exit;

        GetAdditionalFields(TableID, RecordID, Type, AdditionalFields);
        LookupTempTable(City2, PostCode, AdditionalFields."Code serial number Post", County2, AdditionalFields."Additional number ZIP", AdditionalFields."Community number BfS (BFSNR)", CountryRegionCode, UseDialog);
        AdditionalFields.Modify;
    end;

    local procedure ValidatePostCode(TableID: Integer; RecordID: RecordID; Type: Integer; var City2: Text[30]; var PostCode: Code[20]; var County2: Text[30]; var ContryRegionCode: Code[10]; UseDialog: Boolean)
    var
        PostCodeRec: Record "Post Code";
        PostCodeRec2: Record "Post Code";
        AdditionalFields: Record "Additional Fields";
    begin
        if not GuiAllowed or (PostCode = '') then
            exit;

        County2 := '';
        GetAdditionalFields(TableID, RecordID, Type, AdditionalFields);

        if StrPos(PostCode, '*') = StrLen(PostCode) then
            PostCodeRec.SetFilter(Code, PostCode)
        else
            PostCodeRec.SetRange(Code, PostCode);
        if not PostCodeRec.FindFirst then
            exit;

        PostCodeRec2.Copy(PostCodeRec);
        if UseDialog and (PostCodeRec2.Next = 1) then begin
            if PAGE.RunModal(PAGE::"Post Codes", PostCodeRec, PostCodeRec.Code) <> ACTION::LookupOK then
                exit;
        end;

        PostCode := PostCodeRec.Code;
        City2 := PostCodeRec.City;
        if ContryRegionCode = '' then begin
            AdditionalFields."Code serial number Post" := PostCodeRec."Code serial number Post";
            County2 := PostCodeRec.County;
            AdditionalFields."Additional number ZIP" := PostCodeRec."Additional number ZIP";
            AdditionalFields."Community number BfS (BFSNR)" := PostCodeRec."Community number BfS";
            ContryRegionCode := PostCodeRec."Country/Region Code";
        end else begin
            AdditionalFields."Code serial number Post" := '';
            County2 := '';
            AdditionalFields."Additional number ZIP" := '';
            AdditionalFields."Community number BfS (BFSNR)" := '';
        end;

        AdditionalFields.Modify;
    end;
}

