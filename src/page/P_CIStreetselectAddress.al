page 72204 "Streetselect Address"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'Strassenauswahl Adresse';
    DeleteAllowed = false;
    InsertAllowed = false;
    LinksAllowed = false;
    PageType = StandardDialog;
    PopulateAllFields = false;
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            field(StreetName; StreetName)
            {
                ApplicationArea = All;
                Caption = 'Name of Street';

                trigger OnLookup(var Text: Text): Boolean
                begin
                    StreetDirectory.LookupAddress(StreetName, PostCode, PLZZusatz, CountryCode);

                    if (((PostCode <> TempPostCode) and (TempPostCode <> '')) or
                        ((PLZZusatz <> TempPLZZusatz) and (TempPLZZusatz <> '')))
                    then begin
                        if not Confirm(CosiText001, true) then
                            exit;
                    end;

                    StreetDirectory.CreateAdress(Address, StreetName, HouseNo, PostCode);
                end;
            }
            field(HouseNo; HouseNo)
            {
                ApplicationArea = All;
                Caption = 'House No.';

                trigger OnValidate()
                begin
                    StreetDirectory.CreateAdress(Address, StreetName, HouseNo, PostCode);
                end;
            }
            field(Address; Address)
            {
                ApplicationArea = All;
                Caption = 'Adress';

                trigger OnValidate()
                begin
                    StreetDirectory.CreateAdress(Address, StreetName, HouseNo, PostCode);
                end;
            }
        }
    }

    actions
    {
    }

    var
        StreetDirectory: Record "Street Directory M20";
        StreetName: Text[25];
        HouseNo: Text[10];
        Address: Text[30];
        TempStreetName: Text[25];
        PostCode: Code[20];
        TempPostCode: Code[20];
        PLZZusatz: Code[10];
        TempPLZZusatz: Code[10];
        CountryCode: Code[10];
        TempCountryCode: Code[10];
        CosiText001: Label 'Do you want to overwirte the actual values with the new streetdirectory data?';

    [Scope('OnPrem')]
    procedure SetVariables(PostCode2: Code[20]; PLZZusatz2: Code[10]; CountryCode2: Code[10]; StreetName2: Text[30]; HouseNo2: Text[10]; Address2: Text[30])
    begin
        PostCode := PostCode2;
        PLZZusatz := PLZZusatz2;
        CountryCode := CountryCode2;
        StreetName := StreetName2;
        HouseNo := HouseNo2;
        Address := Address2;

        TempPostCode := PostCode;
        TempPLZZusatz := PLZZusatz;
        TempCountryCode := CountryCode;
        TempStreetName := StreetName;
    end;

    [Scope('OnPrem')]
    procedure GetVariables(var StreetName2: Text[30]; var HouseNo2: Text[10]; var Address2: Text[30])
    begin
        StreetName2 := StreetName;
        HouseNo2 := HouseNo;
        Address2 := Address;
    end;
}

