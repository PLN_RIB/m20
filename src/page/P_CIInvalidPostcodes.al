page 72202 "Invalid Postcodes"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'Invalid Postcodes';
    Editable = false;
    InsertAllowed = false;
    ModifyAllowed = false;
    PageType = List;
    SourceTable = "Invalid Postcode";
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code"; Code)
                { ApplicationArea = All; }
                field(City; City)
                { ApplicationArea = All; }
                field("City Code ID"; "City Code ID")
                { ApplicationArea = All; }
                field("Code serial number Post"; "Code serial number Post")
                {
                    ApplicationArea = All;
                    Visible = false;
                }
                field("No. of Contacts"; "No. of Contacts")
                { ApplicationArea = All; }
                field("No. of alt. Addresses"; "No. of alt. Addresses")
                { ApplicationArea = All; }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action(ContactCard)
            {
                ApplicationArea = All;
                Caption = 'Contact Card';
                Image = ContactPerson;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;
                PromotedOnly = true;
                RunObject = Page "Contact Card";
                RunPageLink = "No." = FIELD(Code);
            }
        }
    }
}

