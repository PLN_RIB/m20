page 72203 "ZIP alternative- und area desi"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'ZIP alternative- und area designation';
    Editable = false;
    PageType = List;
    SourceTable = "ZIP Alt. Area Description";
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Code serial number Post"; "Code serial number Post")
                { ApplicationArea = All; }
                field("Sequence number"; "Sequence number")
                { ApplicationArea = All; }
                field("Designation Type"; "Designation Type")
                { ApplicationArea = All; }
                field(Languagecode; Languagecode)
                { ApplicationArea = All; }
                field("Name, 18 digits"; "Name, 18 digits")
                { ApplicationArea = All; }
                field("Name, 27 digits"; "Name, 27 digits")
                { ApplicationArea = All; }
                field("Update Sequence"; "Update Sequence")
                { ApplicationArea = All; }
                field("Mutation status"; "Mutation status")
                { ApplicationArea = All; }
                field("My new ZIP"; "My new ZIP")
                { ApplicationArea = All; }
                field("MY ONRP new ZIP PO box"; "MY ONRP new ZIP PO box")
                { ApplicationArea = All; }
                field("ZIP insinuation new"; "ZIP insinuation new")
                { ApplicationArea = All; }
                field("ONRP new ZIP insinuation"; "ONRP new ZIP insinuation")
                { ApplicationArea = All; }
            }
        }
    }

    actions
    {
        area(processing)
        {
        }
    }
}

