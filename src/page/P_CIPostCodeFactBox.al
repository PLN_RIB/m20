page 72206 "Post Code FactBox"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'Post Code Informations';
    PageType = CardPart;
    SourceTable = "Post Code";

    layout
    {
        area(content)
        {
            field("Alternate- and Area Description"; NoZIPalternativeareadesi)
            {
                Caption = 'Alternate- and Area Description';
                ApplicationArea = All;
                trigger OnLookup(var Text: Text): Boolean
                begin
                    Clear(ZIPalternativeundareadesiPage);
                    ZIPalternativeundareadesiPage.SetTableView(ZIPalternativeareadesi);
                    ZIPalternativeundareadesiPage.Run;
                end;
            }
            field("Post Code Mutations"; NoPLZMutationen)
            {
                Caption = 'Post Code Mutations';
                ApplicationArea = All;
                trigger OnLookup(var Text: Text): Boolean
                begin
                    Clear(AddressinvalidPostcodePage);
                    AddressinvalidPostcodePage.SetTableView(AddressinvalidPostcode);
                    AddressinvalidPostcodePage.Run;
                end;
            }
            field(Streets; NoStreets)
            {
                Caption = 'Streets';
                ApplicationArea = All;
                trigger OnLookup(var Text: Text): Boolean
                begin
                    Clear(StreetDirectoryPage);
                    PostCodeToAddress := CopyStr(Code, 1, 4) + CopyStr("Additional number ZIP", 1, 2);
                    StreetDirectory.SetCurrentKey("Post Code to address", "Name of Street");
                    StreetDirectory.SetRange("Post Code to address", PostCodeToAddress);
                    StreetDirectoryPage.SetTableView(StreetDirectory);
                    if PAGE.RunModal(0, StreetDirectory) = ACTION::LookupOK then;
                end;
            }
            field("Community Numbers BfS"; NoCommunityNumbers)
            {
                Caption = 'Community Numbers BfS';
                ApplicationArea = All;
                trigger OnLookup(var Text: Text): Boolean
                begin
                    Clear(CommunitynumberBfSPage);
                    CommunitynumberBfS.SetRange(Inactive, false);
                    CommunitynumberBfSPage.SetTableView(CommunitynumberBfS);
                    CommunitynumberBfSPage.Run;
                end;
            }
        }
    }

    actions
    {
    }

    trigger OnAfterGetCurrRecord()
    begin
        Clear(ZIPalternativeareadesi);
        ZIPalternativeareadesi.SetRange("Code serial number Post", "Code serial number Post");
        NoZIPalternativeareadesi := ZIPalternativeareadesi.Count;

        Clear(AddressinvalidPostcode);
        NoPLZMutationen := AddressinvalidPostcode.Count;

        Clear(StreetDirectory);
        PostCodeToAddress := CopyStr(Code, 1, 4) + CopyStr("Additional number ZIP", 1, 2);
        StreetDirectory.SetCurrentKey("Post Code to address", "Name of Street");
        StreetDirectory.SetRange("Post Code to address", PostCodeToAddress);
        NoStreets := StreetDirectory.Count;

        Clear(CommunitynumberBfS);
        CommunitynumberBfS.SetRange("No.", "Community number BfS");
        CommunitynumberBfS.SetRange(Inactive, false);
        NoCommunityNumbers := CommunitynumberBfS.Count;
    end;

    var
        AddressinvalidPostcode: Record "Invalid Postcode";
        CommunitynumberBfS: Record "Community number BfS (BFSNR)";
        ZIPalternativeareadesi: Record "ZIP Alt. Area Description";
        StreetDirectory: Record "Street Directory M20";
        AddressinvalidPostcodePage: Page "Invalid Postcodes";
        CommunitynumberBfSPage: Page "Community number BfS (BFSNR)";
        ZIPalternativeundareadesiPage: Page "ZIP alternative- und area desi";
        StreetDirectoryPage: Page "Street Directory M20";
        NoPLZMutationen: Integer;
        NoStreets: Integer;
        NoCommunityNumbers: Integer;
        NoZIPalternativeareadesi: Integer;
        PostCodeToAddress: Code[6];
}

