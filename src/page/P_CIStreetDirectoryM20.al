page 72200 "Street Directory M20"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'Street Directory',
        Comment = 'en-US=Street Directory|de-CH=Strassenverzeichnis|fr-CH=Liste de route';
    Editable = false;
    PageType = List;
    SourceTable = "Street Directory M20";
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("Street No."; "Street No.")
                {
                    ApplicationArea = All;
                }
                field("Base Post Code"; "Base Post Code")
                {
                    ApplicationArea = All;
                }
                field("Name of Street"; "Name of Street")
                {
                    ApplicationArea = All;
                }
                field("Language Code"; "Language Code")
                {
                    ApplicationArea = All;
                }
                field("Name of Street (Uppercase)"; "Name of Street (Uppercase)")
                {
                    ApplicationArea = All;
                }
                field("Post Code to address"; "Post Code to address")
                {
                    ApplicationArea = All;
                }
                field("Split Code"; "Split Code")
                {
                    ApplicationArea = All;
                }
                field("House No. from"; "House No. from")
                {
                    ApplicationArea = All;
                }
                field("House No. from (alpha)"; "House No. from (alpha)")
                {
                    ApplicationArea = All;
                }
                field("House No. to"; "House No. to")
                {
                    ApplicationArea = All;
                }
                field("House No. to (alpha)"; "House No. to (alpha)")
                {
                    ApplicationArea = All;
                }
                field("Street Root"; "Street Root")
                {
                    ApplicationArea = All;
                }
                field("Type of Street"; "Type of Street")
                {
                    ApplicationArea = All;
                }
                field("Preposition Code"; "Preposition Code")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
        }
    }
}

