page 72201 "Community number BfS (BFSNR)"
{
    // +#COSI#
    // 
    // ++ M20 Post Code Directory and Street Directory Switzerland
    // M20 23.10.20 CL : Integration M20
    // 
    // -#COSI#

    Caption = 'Community number BfS (BFSNR)';
    PageType = List;
    SourceTable = "Community number BfS (BFSNR)";
    ApplicationArea = All;
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(Group)
            {
                field("No."; "No.")
                { ApplicationArea = All; }
                field(Name; Name)
                { ApplicationArea = All; }
                field(County; County)
                { ApplicationArea = All; }
                field("Agglomeration number"; "Agglomeration number")
                { ApplicationArea = All; }
                field(Inactive; Inactive)
                { ApplicationArea = All; }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Post Code")
            {
                ApplicationArea = All;
                Caption = 'Post Code';
                Image = AddToHome;
                Promoted = false;
                //The property 'PromotedCategory' can only be set if the property 'Promoted' is set to 'true'
                //PromotedCategory = Process;
                RunObject = Page "Post Codes";
                RunPageLink = "Community number BfS" = FIELD("No.");
                RunPageMode = View;
            }
        }
    }
}

