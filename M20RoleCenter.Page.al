page 72208 "M20RoleCenter"
{
    PageType = RoleCenter;
    Caption = 'M20 Rolecenter';
    actions
    {
        area(Sections)
        {
            group(List)
            {
                Caption = 'Administration';
                action(PostCodes)
                {
                    Caption = 'Post Codes';
                    ApplicationArea = All;
                    RunObject = page "Post Codes";
                }
                action(StreetDirectory)
                {
                    Caption = '';
                    ApplicationArea = All;
                    RunObject = Page "Street Directory M20";
                }
                action(CommunityNumbers)
                {
                    Caption = 'Community Numbers';
                    ApplicationArea = All;
                    RunObject = Page "Community number BfS (BFSNR)";
                }
                action(InvalidPostCodes)
                {
                    Caption = 'Invalid PostCodes';
                    ApplicationArea = All;
                    RunObject = Page "Invalid Postcodes";
                }
                action(AlternativeAdresses)
                {
                    Caption = 'Alternative Addresses';
                    ApplicationArea = All;
                    RunObject = Page "ZIP alternative- und area desi";
                }
            }
            group(Task)
            {
                Caption = 'Tasks';
                action(ImportPostCodes)
                {
                    Caption = 'Import Post Codes';
                    ApplicationArea = All;
                    RunObject = xmlport "Import Match CH";
                }
            }
        }
    }
}